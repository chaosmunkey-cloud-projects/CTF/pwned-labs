# [Breach In The Cloud](https://pwnedlabs.io/labs/breach-in-the-cloud)

This is a blue team lab that focuses on being able to analyse AWS CloudTrail logs from the command line. The goal is to identify the steps an attacker used to gain access to an S3 bucket.

---

## Scenario
> We've been alerted to a potential security incident. The Huge Logistics security team have provided you with AWS keys of an account that saw unusual activity, as well as AWS CloudTrail logs around the time of the activity. We need your expertise to confirm the breach by analyzing our CloudTrail logs, identifying the compromised AWS service and any data that was exfiltrated.


## Steps

### Reformatting the Logs

The first step is to download the zip file for this lab from the Pwned Labs discord server. Unzipping this archive creates 7 log files, which appear to all be JSON formatted.

![](./images/unzip.png)

![](./images/unformatted-json.png)

In its current format, the data within these JSON files isn't really human-readable (difficult to tell where one record stops and the next starts). This is where the tool `jq` comes into play, as it can be used to prettify the data.

![](./images/formatted-json.png)


### Finding the Culprit

Within CloudTrail, the user who initiated an API call is logged and stored under the `username` field. To get an idea of which users have been interacting with this AWS account, the following command can be used to count the number of requests per username:

```bash
jq '.Records[]' 107513503799_CloudTrail_us-east-1_20230826T2* | grep userName | cut -d: -f2 | sort | uniq -c
```

![](./images/user-list.png)

The results show a much higher number of requests coming from the *temp-user* identity when compared to any of the others discovered. This is likely an indicator as to which user performed the attack and the first port of call in the investigation.

To get an understanding of where these requests originated from, we can pull out the IP address from the `sourceIPAddress` field in the logs and count by the number of occurances for each. This can be done with the command:

```bash
jq '.Records[] | select(.userIdentity.userName=="temp-user") | .sourceIPAddress' 107513503799_CloudTrail_us-east-1_20230826T2* | sort -t. -nk4,4 | uniq -c
```

![](./images/source-ips.png)

The results reveal all requests coming from what looks like a `84.32.71.0/24` subnet. Performing some OSINT on this address reveals these IPs originate from Turkey, which, according to the brief, is unusual and points to this being our attacker.

![](./images/ip-lookup.png)


### The Actions

Now that the potential identity that the attacker has gained access to has been identified, it's time to take a look at what actions were performed. Within the data, there is a lot of information that is not relevant or necessary. By using `jq`, it is possible to pull out only fields of interest:

Field|Description
-|-|
`eventTime`   | Time of the event.
`eventSource` | Service request was made to.
`eventName`   | Action requested for the service.
`errorCode`   | If the request failed, this will be populated with the error.

To achieve this the following command can be used:

```bash
jq '.Records[] | select(.userIdentity.userName=="temp-user") | [.eventTime, .eventSource, .eventName, .errorCode]' 107513503799_CloudTrail_us-east-1_20230826T2*
```

To avoid scrolling due to there being over 500 events for this username, the first few events can be seen using the `head` command. As a matter of interest, the useragent has also been added to this particular command output:

![](./images/user-first.png)

The first request made by this identity was likely `aws sts get-caller-identity` based on the user agent. This is similar to using `whoami` on a terminal to see what user you are currently logged in/running as.

Subsequent API calls failed as is evident by the "AccessDenied" error code. This is likely the attacker attempting to work out what permissions this identity has. In this case, it would be better to see only the requests which were successful. This can be achieved with the following command:

```bash
jq '.Records[] | select(.userIdentity.userName=="temp-user" and .errorCode == null)' 107513503799_CloudTrail_us-east-1_20230826T2*
```

The result of this command returns only 3 requests made by this particular identity. Two of which are calls the the `get-caller-identity`, but the last is slightly more worrying due to attacker assuming a role by the name of *AdminRole*.

![](./images/assume-rule.png)

Judging by the name of this role, the attacker will be able to gain elevated permissions.


### AdminRole Activity

Now it's time to investigate the activity of assumed role. According to the search performed earlier, this should return three results. This can be achieved with the following command:

```bash
jq '.Records[] | select(.userIdentity.sessionContext.sessionIssuer.userName=="AdminRole")' 107513503799_CloudTrail_us-east-1_20230826T2*
```

The first event returned by this command is the typical API call `get-caller-identity` which is more than likely to be a sanity check.

The next event returned is the listing of an S3 bucket named **emergency-data-recovery**. Looking at the name, it appears to contain files related to the recovery of data in the event of an emergancy which in itself is a little concerning.

![](./images/admin-s3-ls.png)

The final event is the downloading of a file by the name of **emergency.txt**. In this case, the contents of this file are the flag for this challenge. Outside of this challenge, it could have easily been something sensitive regarding the recovery process or procedure.

![](./images/admin-s3-get.png)

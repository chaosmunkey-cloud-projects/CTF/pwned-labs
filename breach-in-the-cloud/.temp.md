To aid with parsing all of this data in an efficient manner, it can be formatted using the `jq` tool and stored into one file using the following command:

```bash
for file in $(ls *.json);
do
    cat "${file}" | jq >> formatted.json
done
```

![](./images/formatted-json.png)

> There's probably a better way to do this with `jq` itself, but I wasn't able to get any of them to create a parsable files.

# [Reveal Hidden Risks with AWS Security Hub](https://pwnedlabs.io/labs/reveal-hidden-risks-with-aws-security-hub)

## Scenario
> On your first day as a security analyst at Huge Logistics, you activate AWS Security Hub. The next day you are met with a detailed dashboard, revealing a myriad of potential risks within the AWS infrastructure. Use this service to identify risks and help improve their security posture!

---

## Steps
After logging in and navigating to the **Security Hub** dashboard, we are greeted with a number of findings ranging from *Low* all the way up to *Critical*, as well as our environment graded against some well known security standards.

![](./images/security-hub-dashboard.png)


From a cursory glance, there are a number of things that need improving, and the priority should be triaging the critical findings first.


### Critical Findings
#### Open Ports
##### All Ports Open
The first in the list is a more critical one in that it allows all ports (0 to 65535) to be accessed from any IP address. This is incredibly dangerous as it means any service can be accessed if it is assigned this security group.

![](./images/public-all.png)


The solution for this would be to restrict the security group to only allowing ports that need to be publicly accessible to the entire internet. And where possible, restrict it to only a subset of known IP addresses.

This becomes difficult if the service is hosting something like a website, as you may need it to be accessible to everyone on the internet. At this point, the correct monitoring and alerting should be in place to ensure that anything malicious is (hopefully) caught.


##### SSH Publicly Accessible
The first five findings listed as critical have to do with *unrestricted access to ports with high risk*. Basically, this means a security group is allowing access to high risk ports from any incoming IP address.

![](./images/high-risk-ports-finding.png)


Three out of the five findings are for launch configurations where SSH is publically accessible. In an ideal situation, SSH should only be accessible to known IP addresses such as company IP address ranges, or known VPN endpoints (if a company is using a VPN).

![](./images/public-ssh.png)


##### RDS Publicly Accessible
This is an interesting finding in that an RDS database is accessible via the public internet, which in reality should never happen. This is a service which attracts a lot of attention in the form of brute force attacks. These attacks could put a strain on the resources available to the database, and/or fill up the disk with failed access attempts.

![](./images/exposed-rds.png)



What makes this more concerning is the name of the database `employees`. Going just be the name of it, there's a chance it contains sensitive information regarding employees of the company, which, should be private.

The solution would be to place the RDS instance into a private subnet of a VPC with **Security Groups** controlling what services are able to connect to the instance.


#### Publicly Accessible S3 Buckets
A couple of S3 buckets within the account have had their settings set in such a way that the contents of the buckets can be viewed by anyone on the internet.

![](./images/security-hub-s3.png)


One of the buckets in the list has the bucket name of *huge-logistics.com*. This tells me this is likely a bucket which is being used to host a static website, and probably not one to worry about.

The other bucket on the other hand is a little more concerning. This one goes by the name of *huge-logistics-export-temp*. This account has been given access to view the content of this bucket, and it contains a set of access keys, software licence keys and the flag to this lab.

![](./images/s3-bucket-contents.png)
![](./images/s3-licence-keys.png)
![](./images/s3-access-keys.png)

The file named `migration_accessKeys.csv` appears to contain some valid access keys.
![](./images/migration-access-keys.png)


#### Other Critical Findings
 - Root account with access keys: The root account should only be used when first setting up the account, or in an emergency. Having access keys to the *god* account is not advisable as it gives someone unfettered access to you account.
 - EBS snapshots publicly restorable: EBS snapshots should not be available to anyone as they could contain sensitive company information, and deleted files.
